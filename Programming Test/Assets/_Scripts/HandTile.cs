﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandTile : MonoBehaviour
{
	public Text tileValueText;
    public int handTileNumber;

    int tileValue = 0;

    void Start()
    {
        GameManager.Instance.tileManager.PlayHandEvent += UpdateTileValue;
        UpdateTileValue();
    }

    void UpdateTileValue()
    {
        if(GameManager.Instance.tileManager.currentDeck.Count > handTileNumber)
        {
            tileValue = GameManager.Instance.tileManager.currentDeck[handTileNumber];
            tileValueText.text = tileValue.ToString();
        }
        else
        {
            RemoveTile();
        }
    }

    public void OnClick()
    {
        GameManager.Instance.tileManager.QueueTile(handTileNumber, tileValue);
    }

    public void RemoveTile()
    {
        this.gameObject.SetActive(false);
    }


}
