﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveTile : MonoBehaviour
{
    public Text tileValueText;
    public Text tileSumsText;
	public int tileValue = 0;
    public int row;
    public int column;
    public LayerMask activeTileRayCheck;

    int tilesSum = 0;
    Vector3[] directions;

    void Awake()
    {
        tileValue = GameManager.Instance.tileManager.AssignTileValueToActiveTile();
        directions = new Vector3[] {Vector3.up, Vector3.down, Vector3.left, Vector3.right};
    }

    void Start()
    {
        tilesSum += tileValue;
        TriggerSumsOfTiles();
        UpdateTileTexts();
    }

    void TriggerSumsOfTiles()
    {
        RaycastHit hit;
        for(int i = 0; i < 4; i++)
        {
            if(Physics.Raycast(transform.position, directions[i] , out hit, 1.0f, activeTileRayCheck))
            {
                ActiveTile tile = hit.collider.gameObject.GetComponent<ActiveTile>();
                tilesSum += tile.tileValue;
                tile.UpdateSumOnThisTile(tileValue);
            }
        }
    }

    public void UpdateSumOnThisTile(int value)
    {
        tilesSum += value;
        UpdateTileTexts();
    }

    public void SetRowAndColumn(int row_, int column_)
    {
        row = row_;
        column = column_;
    }

    void UpdateTileTexts()
    {
        tileValueText.text = tileValue.ToString();
        tileSumsText.text = tilesSum.ToString();
    }

}
