﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
	float minZoom = 15f;
    float maxZoom = 90f;
    float sensitivity = 35f;

    Camera mainCamera;

    void Start()
    {
        mainCamera = GetComponent<Camera>();
    }

    void Update()
    {
        float fov = mainCamera.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        fov = Mathf.Clamp(fov, minZoom, maxZoom);
        mainCamera.fieldOfView = fov;
    }
}
