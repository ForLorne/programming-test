﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TilePlacement : MonoBehaviour
{
    public GameObject activeTile;

    MeshRenderer mRenderer;
    Color32 transparentTile = new Color32(255, 255, 255, 0);
    Color32 visibleTile = new Color32(255, 255, 255, 75);

    public int row;
    public int column;

    void Start()
    {
        mRenderer = GetComponent<MeshRenderer>();
        mRenderer.material.color = transparentTile;

        if(row == 9 && column == 9) FirstTile();
    }

    void OnMouseOver()
    {
        mRenderer.material.color = visibleTile;
    }

    void OnMouseExit()
    {
        mRenderer.material.color = transparentTile;
    }

    void OnMouseDown()
    {
        if(EventSystem.current.IsPointerOverGameObject()){
            return;
        }
            
        if(GameManager.Instance.tileManager.ElligibleForTilePlacement(row, column) && CheckForAdjacentTiles())
        {
            PlaceTile();
        }
    }


    public bool CheckForAdjacentTiles()
    {
        if(ConfirmWithinBounds(row - 1, column) && GameManager.Instance.tileManager.tilePlaced[row - 1, column]) return true;
        if(ConfirmWithinBounds(row + 1, column) && GameManager.Instance.tileManager.tilePlaced[row + 1, column]) return true;
        if(ConfirmWithinBounds(row, column + 1) && GameManager.Instance.tileManager.tilePlaced[row, column + 1]) return true;
        if(ConfirmWithinBounds(row, column - 1) && GameManager.Instance.tileManager.tilePlaced[row, column - 1]) return true;

        return false;
    }

    bool ConfirmWithinBounds(int row_, int column_)
    {
        if(row_ < 0 || row_ > GameManager.Instance.tileManager.tilePlaced.GetLength(0) - 1) return false;
        if(column_ < 0 || column_ > GameManager.Instance.tileManager.tilePlaced.GetLength(1) - 1) return false;

        return true;

    }

    void PlaceTile()
    {
        GameObject iTile = Instantiate(activeTile, this.transform.position, Quaternion.identity);
        iTile.name = "Active Tile " + row + ", " + column;
        iTile.GetComponent<ActiveTile>().SetRowAndColumn(row, column);
        GameManager.Instance.tileManager.tilePlaced[row, column] = true;
        GameManager.Instance.tileManager.PlayHandTile();
        Destroy(this.gameObject);
    }

    void FirstTile()
    {
        GameObject iTile = Instantiate(activeTile, this.transform.position, Quaternion.identity);
        iTile.name = "Active Tile " + row + ", " + column;
        iTile.GetComponent<ActiveTile>().SetRowAndColumn(row, column);
        GameManager.Instance.tileManager.tilePlaced[row, column] = true;
        Destroy(this.gameObject);
    }
}
