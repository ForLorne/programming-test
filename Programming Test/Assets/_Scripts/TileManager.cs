﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    public GameObject mouseoverTile;
    public int queuedTileValue = -1;
    public int queuedHandTile = -1;
    public int[] tileHand = new int[3];
	public List<int> currentDeck;

    float tileHeight;
    float tileWidth;
    float XOffsetTileSpawnPosition;
    float YOffsetTileSpawnPosition;
    Vector3 vectorOffsetSpawnTilePosition;
    float tileZPosition = 0.0f;
    Vector3[,] tileLocations = new Vector3[19,19];
    public bool[,] tilePlaced = new bool[19,19];
    GameObject tileFolder;

    public delegate void PlayHand();
    public PlayHand PlayHandEvent;

    void Awake()
    {
        tileHeight = mouseoverTile.transform.localScale.y;
        tileWidth = mouseoverTile.transform.localScale.x;
        XOffsetTileSpawnPosition = -tileWidth * 9f;
        YOffsetTileSpawnPosition = tileHeight * 9f;
        vectorOffsetSpawnTilePosition = new Vector3(XOffsetTileSpawnPosition, YOffsetTileSpawnPosition, tileZPosition);
        GenerateNewDeck();
        PrepFirstTile();
    }

    void Start()
    {
        GenerateNewPlayarea();
    }

    void GenerateNewDeck()
    {
        currentDeck = new List<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
       
        for(int i = 0; i < currentDeck.Count; i++)
        {
            int temp = currentDeck[i];
            int r = Random.Range(i, currentDeck.Count);
            currentDeck[i] = currentDeck[r];
            currentDeck[r] = temp;
        }
    }

    void GenerateNewPlayarea()
    {
        ClearPlayarea();
        CalculateTilePositions();

        tileFolder = Instantiate(new GameObject(), Vector3.zero, Quaternion.identity);
        tileFolder.gameObject.name = "Tile Folder";
        
        for(int i = 0; i < tileLocations.GetLength(0); i++)
        {
            for(int j = 0; j < tileLocations.GetLength(1); j++)
            {
                GameObject k = Instantiate(mouseoverTile, tileLocations[i, j] + vectorOffsetSpawnTilePosition, Quaternion.identity);
                k.transform.parent = tileFolder.transform;
                k.gameObject.name = "Tile slot " + i + ", " + j;
                TilePlacement tilePlacement = k.GetComponent<TilePlacement>();
                tilePlacement.row = i;
                tilePlacement.column = j;
            }
        }
    }

    void CalculateTilePositions()
    {
        for(int i = 0; i < tileLocations.GetLength(0); i++)
        {
            for(int j = 0; j < tileLocations.GetLength(1); j++)
            {
                tileLocations[i,j] = new Vector3(j * tileWidth, i * -tileHeight, tileZPosition);
            }
        }
    }

    void ClearPlayarea()
    {
        if(tileFolder == null) return;

        foreach(Transform child in tileFolder.transform)
        {
            Destroy(child.gameObject);
        }

        Destroy(tileFolder);

        for(int i = 0; i < tilePlaced.GetLength(0); i++)
        {
            for(int j = 0; j < tilePlaced.GetLength(1); j++)
            {
                tilePlaced[i, j] = false;
            }
        }
    }

    public int AssignTileValueToActiveTile()
    {
        int temp = queuedTileValue;
        queuedTileValue = -1;
        return temp;
    }

    public void PlayHandTile()
    {
        currentDeck.RemoveAt(queuedHandTile);
        queuedHandTile = -1;
        if(PlayHandEvent != null) PlayHandEvent();
    }

    public void QueueTile(int handTileNumber, int handTileValue)
    {
        queuedHandTile = handTileNumber;
        queuedTileValue = handTileValue;
    }

    public void PrepFirstTile()
    {
        queuedTileValue = currentDeck[currentDeck.Count -1];
        currentDeck.RemoveAt(currentDeck.Count -1);
    }

    public bool ElligibleForTilePlacement(int row_, int column_)
    {
        return (!tilePlaced[row_, column_] && queuedHandTile != -1);
    }

}
